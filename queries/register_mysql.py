#!/usr/bin/python
import MySQLdb
import sys
import pprint
import json
import sys

single_categories = {
	'cat_ade': 'Administracao Direta Estadual',
	'cat_pri': 'Privada-Instrucao',
	'cat_sae': 'Servico Aereo Especializado Publico',
	'cat_sae_mu': 'Multipla Categoria SAE',
	'cat_sae_aa': 'Privada Serv.Aereo Especializado Publico-Apoio Aereo (AA)',
	'cat_sae_ac': 'Privada-Serv.Aereo Especializado.Pub.Aerocinematografia (AC)',
	'cat_sae_ad': 'Privada - Serv.Aereo Esp.Publico-Aerodemonstracao',
	'cat_sae_af': 'Privada Serv.Aereo Especializado Publico Aerofotografia (AF)',
	'cat_sae_ag': 'Privada Serv.Aereo Especializado Publico Aeroagricola (AG)',
	'cat_sae_ai': 'Privada serv.aereo Especializado Publico-combate a incendios',
	'cat_sae_al': 'Privada Serv.Aereo Especializado Publico Aerolevantamento (AL)',
	'cat_sae_an': 'Privada Serv.Aereo Especializado Publico-Aeroinspecao',
	'cat_sae_ap': 'Privada Serv.Aereo Especializado Publico Aeropublicidade (AP)',
	'cat_sae_ar': 'Privada Serv.Aereo Especializado Publico Aeroreportagem (AR)',
	'cat_prh': 'Privada Historica',
	'cat_tpn': 'Transporte Aereo Publico nao Regular',
	'cat_tpp': 'Servico Aereo Privado',
	'cat_tpr': 'Transporte Aereo Publico Regular',
	'cat_tpx': 'Transporte Publico nao Regular',
	'cat_aie': 'Administracao Indireta Estadual',
	'cat_pin': 'Publica Instrucao',
	'cat_adf': 'Administracao Direta Federal',
	'cat_add': 'Administracao Direta do Distrito Federal',
	'cat_adm': 'Publica Administracao Direta Municipal',
	'cat_aif': 'Administracao Indireta Federal',
	'cat_puh': 'Publica Historica',
	'cat_aid': 'Privada Adm. Indireta Distrito Federal'
}

multiple_categories = ['Dupla categoria-TPX/SAE-AA',
					   'Dupla categoria-TPX/SAE-AC',
					   'DUPLA CATEGORIA-TPX/SAE-AD',
					   'Dupla categoria-TPX/SAE-AF',
					   'Dupla categoria-TPX/SAE-AG',
					   'Dupla categoria-TPX/SAE-AI',
					   'Dupla categoria-TPX/SAE-AL',
					   'Dupla categoria-TPX/SAE-AN',
					   'Dupla categoria-TPX/SAE-AP',
					   'Dupla categoria-TPX/SAE-AR',
					   'Multipla categoriaTPX/SAE-AA/C/F/I/N/R',
					   'Multipla categoria TPX/SAE-AA/C/F/P/R',
					   'Multipla categoria TPX/SAE-AF/N',
					   'Multipla categoria TPX/SAE/-AA/C/F/N/P/R',
					   'Multipla categoria TPX/SAE-AA/F/P/R',
					   'Multipla Categoria TPX/SAE-AA/C (OBS.COD.CA=13)',
					   'Multipla categoria TPX/SAE-AA/C/F/R (OBS.COD.CA = 22)',
					   'Multipla categoria TPX/SAE-AC/F/P/R (OBS.COD.CA = 23)',
					   'Multipla categoria TPX/SAE/AC/F/N/P/R (OBS.COD.CA = 24)',
					   'Multipla categoria TPX/SAE-AC/F/I/N/R(OBS.COD.CA=26)',
					   'Multipla categoria TPX/SAE-AC/F/R(OBS.COD.CA=28)',
					   'Multipla categoria TPX/SAE/AL/N (OBS.COD.CA=29)',
					   'Multipla categoria TPX/SAE-AFr',
					   'Multipla categoria TPX/SAE-AC/F/I/N/P/R (OBS.COD.CA = 47)',
					   'Multipla categoria TPX/SAE-AP/R (OBS.COD.CA = 50)',
					   'Multipla categoria TPX/SAE-AC/D/F/I/L/N/P/R (OBS.COD.CA=52)',
					   'Multipla categoria TPX/SAE-AA/C/D/F/I/L/N/P/R (OBS.COD.CA=54',
					   'Multipla categoria TPX/SAE-AA/F/I/P/R (OBS.COD.CA=55)',
					   'Multipla categoria TPX/SAE-AA/C/F/N/R (OBS.COD.CA=54)',
					   'Multipla categoria TPX/SAE-AL/P (OBS.COD.CA = 57)']

###############################################################################
# Dismantles categories with multiple values
def define_categories(cat_val):
	categories_str = "" 		# This will store the names of the categories according to the 'aeronaves' table
	categories_markers = ""		# This will store wheter the category is true or false given a plane
	is_this_category = '0'	# This stores the presence of a single category

	mc = map(lambda s: s.upper(), multiple_categories)
	cat_val = cat_val.upper() 	# Make sure value from file is all uppercase

	if cat_val in mc: # if it is a multiple category
		saes_present = cat_val.split('TPX/SAE-A')[1].split(' ')[0].split('(')[0].split('/') # Get specific SAEs where it is included
		saes_categories = filter(lambda c: 'cat_sae_a' in c, single_categories)

		for category in saes_categories:
			if category[-1:].upper() in saes_present:
				is_this_category = '1'
			else:
				is_this_category = '0'
			categories_str += category + ", "
			categories_markers += "\'" + is_this_category + "\'" + ", "

		for category in list(set(single_categories) - set(saes_categories)):
			if category == 'cat_tpx':
				is_this_category = '1'
			else:
				is_this_category = '0'
			categories_str += category + ", "
			categories_markers += "\'" + is_this_category + "\'" + ", "
	else: # If it is a single category
		for category, description in single_categories.iteritems():
			if description.upper() in cat_val:
				is_this_category = '1'
			else:
				is_this_category = '0'
			categories_str += category + ", "
			categories_markers += "\'" + is_this_category + "\'" + ", "

	return categories_str, categories_markers

# Prepares a SQL entry query from a python dictionary
def prepare_entry_query(dic):
	columns_str = ""
	values_str = ""
	for key in dic:
		if dic[key] != None:
			columns_str += key + ", "
			values_str += "\'" + dic[key].replace("'", "_").replace("\"", "_") + "\'" + ", "
			if key == 'categoria_de_registro':
				categories, categories_markers = define_categories(dic[key])
				columns_str += categories
				values_str += categories_markers
	return columns_str.strip(", "), values_str.strip(", ")

###############################################################################
def main():
	db = MySQLdb.connect(host='localhost',
						 user='root',
						 passwd='sql@VINCI',
						 db='eyeson')

	# db.cursor will return a cursor object, you can use this cursor to perform queries
	cursor = db.cursor()

	planes_f = open(sys.argv[1], 'r')

	cursor.execute("""CREATE TABLE aircraft (id INT NOT NULL AUTO_INCREMENT, matricula VARCHAR(5), proprietario VARCHAR(60), cpf_ou_cgc_prop VARCHAR(15), operador VARCHAR(60), cpf_ou_cgc_oper VARCHAR(15), fabricante VARCHAR(50), modelo VARCHAR(30), numero_de_serie VARCHAR(20), tipo_icao VARCHAR(10), tipo_de_habilitacao_para_pilotos VARCHAR(10), classe_da_aeronave VARCHAR(100), peso_maximo_de_decolagem VARCHAR(20), numero_maximo_de_passageiros VARCHAR(4), categoria_de_registro VARCHAR(100), numero_dos_certificados__cm___ca VARCHAR(10), situacao_no_rab VARCHAR(60), data_da_compra_ou_transferencia VARCHAR(10), data_de_validade_do_ca VARCHAR(10), data_de_validade_da_iam VARCHAR(10), situacao_de_aeronavegabilidade VARCHAR(60), motivo_s VARCHAR(255), data_de_validade_do_rca VARCHAR(10), cat_ade BOOLEAN, cat_pri BOOLEAN, cat_sae BOOLEAN, cat_sae_aa BOOLEAN, cat_sae_ac BOOLEAN, cat_sae_ad BOOLEAN, cat_sae_af BOOLEAN, cat_sae_ag BOOLEAN, cat_sae_ai BOOLEAN, cat_sae_al BOOLEAN, cat_sae_an BOOLEAN, cat_sae_ap BOOLEAN, cat_sae_ar BOOLEAN, cat_prh BOOLEAN, cat_tpn BOOLEAN, cat_tpp BOOLEAN, cat_tpr BOOLEAN, cat_tpx BOOLEAN, cat_aie BOOLEAN, cat_pin BOOLEAN, cat_adf BOOLEAN, cat_add BOOLEAN, cat_adm BOOLEAN, cat_aif BOOLEAN, cat_puh BOOLEAN, cat_aid BOOLEAN, cat_sae_mu BOOLEAN, air_shuttle BOOLEAN, prop_norm VARCHAR(60), oper_norm VARCHAR(60), fab_norm VARCHAR(50), modelo_norm VARCHAR(50), tipo_de_voo_autorizado VARCHAR(50), PRIMARY KEY (id) )""")
	for line in planes_f:
		columns, values = prepare_entry_query(json.loads(line))

		# execute our Query
		cursor.execute("INSERT INTO aircraft (" + columns + ") VALUES (" + values + ")")
	cursor.execute("ALTER TABLE `aircraft` CHANGE `matricula` `registry` VARCHAR(5) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NULL DEFAULT NULL;")
	db.commit()
	db.close()

if __name__ == "__main__":
	main()