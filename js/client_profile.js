function call_user_profile () {
    $('#eo-page-content').load(
        'view/profile.html',
        function () {
            get_user_info();
        }
    );
}

function get_user_info () {
    $.ajax({
        type: 'post',
        url: apiUrl+'get/profile.php',
        beforeSend: function (xhr) {
            xhr.setRequestHeader ("Authorization", "Bearer " + EyesOn.common.JWT);
        },
        success: function (result) {
            var data = JSON.parse(result);

            if (data.hasOwnProperty('error')) {
                swal("Error", "Server error on users list request", "error");
            } else {
                for (var property in data) {
                    if(data.hasOwnProperty(property)){
                        $('#'+property).html(data[property]);
                    }
                }

                prepare_update_user_btn(data);
            }
        }
    });
}

function prepare_update_user_btn (user) {
    $('#update_user_btn').click(function () {
        $.ajax({
            type: 'GET',
            url: 'update_form/client_profile.html'
        }).done(function (response) {
            $('#eo-page-content').html(response);
            EyesOn.common.loadFormElements();

            $('#user_name').val(user.name);
            $('#user_email').val(user.email);
            $('#user_phone').val(user.tel);
            $('#user_phone').attr('placeholder',user.tel);
            $('#user_role').val(user.role);

            enable_update_profile();
        });
    });

    $('#update_pwd_btn').click(function () {
        $.ajax({
            type: 'GET',
            url: 'update_form/password.html'
        }).done(function (response) {
            $('#eo-page-content').html(response);
            EyesOn.common.loadFormElements();

            $('#user_name').val(user.name);
            $('#user_email').val(user.email);
            $('#user_phone').val(user.tel);
            $('#user_phone').attr('placeholder',user.tel);
            $('#user_company').val(user.company_id);
            $('#user_role').val(user.role);

            enable_update_password();
        });
    });
}

function enable_update_profile () {
     $('#btn_confirm_update').click(function () {
        
        $.ajax({
            url: apiUrl+'update/profile.php',
            method: 'post',
            data: $('#update_user_form').serialize(),
            beforeSend: function (xhr) {
                xhr.setRequestHeader ("Authorization", "Bearer " + EyesOn.common.JWT);
            },  
            success : function (response) {
                swal("Success", "Profile has been updated", "success");
                call_user_profile();
            },
            error: function (response) {
                swal("Canceled", "There was an error with your request: " + response, "error");
            }
        });
        
    });

    $('#btn_cancel_update').click(function () {
        call_user_profile();
    });
}

function enable_update_password () {
     $('#btn_confirm_update').click(function () {
        
        $.ajax({
            url: apiUrl+'update/password.php',
            method: 'post',
            data: $('#password_form').serialize(),
            beforeSend: function (xhr) {
                xhr.setRequestHeader ("Authorization", "Bearer " + EyesOn.common.JWT);
            },  
            success : function (response) {
                EyesOn.events.successAlert('update', 'password');
                call_user_profile();
            },
            error: function (response) {
                swal("Canceled", "There was an error with your request: " + response.responseJSON.error, "error");
            }
        });
        
    });

    $('#btn_cancel_update').click(function () {
        call_user_profile();
    });
}
