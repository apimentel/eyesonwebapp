$(document).ready(function(){
    requestUserName();
    loadMainPageContent();
    prepareBugReportButton();
    prepareSidebarHighlight();

    prepareOrdinarySidebarItems([
        'inspection',
        'base',
        'employee',
        'commercial_condition',
        'company',
        'operator',
        'scope'
    ]);

    prepareUsersSidebarItem();
    prepareCalendarSidebarItem();
    prepareAircraftSidebarItem();
    prepareUserProfileSidebarItem();
    prepareReportSidebarItem();
});
