function call_user_company () {
    $('#eo-page-content').load(
        'view/my_company.html',
        function () {
            get_company_info();
        }
    );
}

function get_company_info () {
    $.ajax({
        type: 'post',
        url: apiUrl+'get/my_company.php',
        beforeSend: function (xhr) {
            xhr.setRequestHeader ("Authorization", "Bearer " + EyesOn.common.JWT);
        },  
        success: function (result) {
            var data = JSON.parse(result);

            if (data.hasOwnProperty('error')) {
                swal("Error", "Server error on users list request", "error");
            } else {
                for (var property in data) {
                    if(data.hasOwnProperty(property)){
                        $('#'+property).html(data[property]);
                    }
                }

                configureButton(data);
            }
        }
    });
}



function configureButton (data) {
    // Prepare update buttons
    $('#update_company_btn').click(function () {
        $.ajax({
            type: 'GET',
            url: 'update_form/my_company.html',
        }).done(function (response) {
            initializeUpdateForm(response, data);
        });

    });    
};

function initializeUpdateForm (form, entryToUpdate) {
	// var self = this;
	// Load form
	$('#eo-page-content').html(form);

    // Load plugins and auto-populated elements
    EyesOn.common.loadFormElements(entryToUpdate);
    
    // On Confirm Button click, send form info to update.php
	$('#eo-confirm-btn').click(function () {
        $.ajax({
            url: apiUrl+'update/company.php',
            method: 'POST',
            beforeSend: function (xhr) {
                xhr.setRequestHeader ("Authorization", "Bearer " + EyesOn.common.JWT);
            },  
            data: 'id='+entryToUpdate.id+'&'+$('#update_company_form').serialize()+'&description='+$('.summernote').summernote('code'),
            success : function (response) {
                var data = JSON.parse(response);

                if (data.hasOwnProperty('error')) {
                    EyesOn.events.errorAlert(data.error);
                } else {
                    EyesOn.events.successAlert('update', 'my_company');
                }
                call_user_company();
            },
            error: function (response) {
                EyesOn.events.errorAlert(response);
            }
        });
        
    });
    
    $('.eo-cancel-btn').click(function () {
        call_user_company();
    });

};
