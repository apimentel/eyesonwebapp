document.write('</li>\
\
                </ul> <!-- /Items List -->\
            </div> <!-- /Sidebar-collapse -->\
        </nav><!-- /Lateral Navigation Bar -->\
\
        <!-- Page -->\
        <div class="gray-bg" id="page-wrapper">\
            <!-- Upper Navigation Bar -->\
            <div class="row border-bottom">\
                <nav class="navbar navbar-static-top" role="navigation" style="margin-bottom: 0">\
                    <div class="navbar-header">\
                        <a class="navbar-minimalize minimalize-styl-2 btn btn-primary" href="#"><i class="fa fa-bars"></i></a>\
                    </div>\
                    <ul class="nav navbar-top-links navbar-right">\
                        <li>\
                            <a href="#eo-modal-report-bug" data-toggle="modal" id="eo-report-bug"><i class="fa fa-bug"></i> Report a Bug</a>\
                        </li>\
                        <li>\
                            <a href="#" id="eo-logout-btn"><i class="fa fa-sign-out"></i> Log out</a>\
                        </li>\
                    </ul>\
                </nav>\
            </div> <!-- /Upper Navigation Bar -->\
\
            <!-- Area under Navigation Bar -->\
            <div class="wrapper wrapper-content animated fadeInRight">\
                <div class="row">\
                    <div id="eo-page-content">\
                        <div class="middle-box text-center animated fadeInRightBig">\
                            <h3 class="font-bold">Welcome to VINCI EyesOn&copy;</h3>\
                            <div class="error-desc">\
                                Your page is loading\
                            </div>\
                        </div>\
                    </div>\
\
                    <div id="eo-modal-change-status" class="modal fade" aria-hidden="true">\
                        <div class="modal-dialog">\
                            <div class="modal-content">\
                                <div class="modal-body">\
                                    <div class="sk-spinner sk-spinner-double-bounce">\
                                        <div class="sk-double-bounce1"></div>\
                                        <div class="sk-double-bounce2"></div>\
                                    </div>\
                                    <div class="row">\
                                        <h3 class="m-t-none m-b">Change Status</h3>\
                                    </div>\
                                    <div class="row">\
                                        <div class="col-sm-12">\
                                            <form id="eo-inspection-status-modal-form" role="form">\
                                                <div class="row">\
                                                    <div class="form-group">\
                                                        <select class="eo-list-inspection_status" name="status" style="width: 100%;"></select>\
                                                    </div>\
                                                </div>\
                                                <div id="eo-air-num-wrapper" class="row p-w-sm" style="display: none;">\
                                                    <label class="control-label col-sm-2">AIR Number</label>\
                                                    <div class="col-sm-4"><input class="form-control eo-input-air" type="num" name="air"></div>\
                                                </div>\
                                                <div class="row">\
                                                    <div class="form-group">\
                                                        <button class="btn btn-primary btn-lg eo-confirm-btn" type="button"><i class="fa fa-check"></i></button>\
                                                        <button class="btn btn-default btn-lg eo-cancel-btn" type="button">Cancel</button>\
                                                    </div>\
                                                </div>\
                                            </form>\
                                        </div>\
                                    </div>\
                                </div>\
                            </div>\
                        </div>\
                    </div>\
\
                    <div id="eo-modal-report-bug" class="modal fade" aria-hidden="true">\
                        <div class="modal-dialog">\
                            <div class="modal-content">\
                                <div class="modal-body">\
                                    <div class="sk-spinner sk-spinner-double-bounce">\
                                        <div class="sk-double-bounce1"></div>\
                                        <div class="sk-double-bounce2"></div>\
                                    </div>\
                                    <div class="row">\
                                        <h3 class="m-t-none m-b">Report a Bug/Problem</h3>\
                                    </div>\
                                    <div class="row">\
                                        <div class="col-sm-12">\
                                            <form id="eo-report-bug-modal-form" role="form">\
                                                <div class="row">\
                                                    <label>What happened?</label>\
                                                    <textarea rows="10" cols="50" id="eo-input-description" placeholder="Please describe your problem with as much detail as possible" class="form-control required" type="text" name="description" required></textarea>\
                                                </div>\
                                                <div class="row m-t-sm">\
                                                    <label>Add Image</label>\
                                                    <div class="fileinput fileinput-new input-group" data-provides="fileinput">\
                                                        <div class="form-control" data-trigger="fileinput">\
                                                            <i class="glyphicon glyphicon-file fileinput-exists"></i>\
                                                        <span class="fileinput-filename"></span>\
                                                        </div>\
                                                        <span class="input-group-addon btn btn-default btn-file">\
                                                            <span class="fileinput-new">Select file</span>\
                                                            <span class="fileinput-exists">Change</span>\
                                                            <input type="file" id="#eo-input-bug_image" name="bug_image" accept="image/*"/>\
                                                        </span>\
                                                        <a href="#" class="input-group-addon btn btn-default fileinput-exists" data-dismiss="fileinput">Remove</a>\
                                                    </div> \
                                                </div> \
                                                <div class="row m-t-sm">\
                                                    <div class="form-group">\
                                                        <button class="btn btn-primary btn-lg eo-confirm-btn" type="button"><i class="fa fa-check"></i></button>\
                                                        <button class="btn btn-default btn-lg eo-cancel-btn" type="button">Cancel</button>\
                                                    </div>\
                                                </div>\
                                            </form>\
                                        </div>\
                                    </div>\
                                </div>\
                            </div>\
                        </div>\
                    </div>\
                </div>\
\
            </div><!-- /Area under Navigation Bar -->\
            \
            <div class="footer fixed">\
                <div class="pull-right"><strong>Copyright</strong> &copy; 2014-2017 Vinci Aeronáutica </div>\
            </div>\
\
            \
        </div><!-- /Page -->\
        \
    </div><!-- /wrapper -->\
');
