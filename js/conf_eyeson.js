// Application namespace
var EyesOn = EyesOn || {};

window.mobilecheck = function() {
    var check = false;
    (function(a){if(/(android|bb\d+|meego).+mobile|avantgo|bada\/|blackberry|blazer|compal|elaine|fennec|hiptop|iemobile|ip(hone|od)|iris|kindle|lge |maemo|midp|mmp|mobile.+firefox|netfront|opera m(ob|in)i|palm( os)?|phone|p(ixi|re)\/|plucker|pocket|psp|series(4|6)0|symbian|treo|up\.(browser|link)|vodafone|wap|windows ce|xda|xiino/i.test(a)||/1207|6310|6590|3gso|4thp|50[1-6]i|770s|802s|a wa|abac|ac(er|oo|s\-)|ai(ko|rn)|al(av|ca|co)|amoi|an(ex|ny|yw)|aptu|ar(ch|go)|as(te|us)|attw|au(di|\-m|r |s )|avan|be(ck|ll|nq)|bi(lb|rd)|bl(ac|az)|br(e|v)w|bumb|bw\-(n|u)|c55\/|capi|ccwa|cdm\-|cell|chtm|cldc|cmd\-|co(mp|nd)|craw|da(it|ll|ng)|dbte|dc\-s|devi|dica|dmob|do(c|p)o|ds(12|\-d)|el(49|ai)|em(l2|ul)|er(ic|k0)|esl8|ez([4-7]0|os|wa|ze)|fetc|fly(\-|_)|g1 u|g560|gene|gf\-5|g\-mo|go(\.w|od)|gr(ad|un)|haie|hcit|hd\-(m|p|t)|hei\-|hi(pt|ta)|hp( i|ip)|hs\-c|ht(c(\-| |_|a|g|p|s|t)|tp)|hu(aw|tc)|i\-(20|go|ma)|i230|iac( |\-|\/)|ibro|idea|ig01|ikom|im1k|inno|ipaq|iris|ja(t|v)a|jbro|jemu|jigs|kddi|keji|kgt( |\/)|klon|kpt |kwc\-|kyo(c|k)|le(no|xi)|lg( g|\/(k|l|u)|50|54|\-[a-w])|libw|lynx|m1\-w|m3ga|m50\/|ma(te|ui|xo)|mc(01|21|ca)|m\-cr|me(rc|ri)|mi(o8|oa|ts)|mmef|mo(01|02|bi|de|do|t(\-| |o|v)|zz)|mt(50|p1|v )|mwbp|mywa|n10[0-2]|n20[2-3]|n30(0|2)|n50(0|2|5)|n7(0(0|1)|10)|ne((c|m)\-|on|tf|wf|wg|wt)|nok(6|i)|nzph|o2im|op(ti|wv)|oran|owg1|p800|pan(a|d|t)|pdxg|pg(13|\-([1-8]|c))|phil|pire|pl(ay|uc)|pn\-2|po(ck|rt|se)|prox|psio|pt\-g|qa\-a|qc(07|12|21|32|60|\-[2-7]|i\-)|qtek|r380|r600|raks|rim9|ro(ve|zo)|s55\/|sa(ge|ma|mm|ms|ny|va)|sc(01|h\-|oo|p\-)|sdk\/|se(c(\-|0|1)|47|mc|nd|ri)|sgh\-|shar|sie(\-|m)|sk\-0|sl(45|id)|sm(al|ar|b3|it|t5)|so(ft|ny)|sp(01|h\-|v\-|v )|sy(01|mb)|t2(18|50)|t6(00|10|18)|ta(gt|lk)|tcl\-|tdg\-|tel(i|m)|tim\-|t\-mo|to(pl|sh)|ts(70|m\-|m3|m5)|tx\-9|up(\.b|g1|si)|utst|v400|v750|veri|vi(rg|te)|vk(40|5[0-3]|\-v)|vm40|voda|vulc|vx(52|53|60|61|70|80|81|83|85|98)|w3c(\-| )|webc|whit|wi(g |nc|nw)|wmlb|wonu|x700|yas\-|your|zeto|zte\-/i.test(a.substr(0,4))) check = true;})(navigator.userAgent||navigator.vendor||window.opera);
    return check;
};

EyesOn.common = {
    JWT: '',

    inspectionStatus: {
        'Aircraft Inspection Cancelled': {
            calendarColor: '#dd0000',
            shortName: 'Cancelled',
            buttonType: 'label-danger'
        },
        'Processing AIA': {
            calendarColor: '#ff7400',
            shortName: 'Processing AIA',
            buttonType: 'label-warning'
        },
        'Aircraft Inspection scheduled': {
            calendarColor: '#12bfbf',
            shortName: 'Scheduled',
            buttonType: 'label-primary'
        },
        'AIR available on Dataroom': {
            calendarColor: '#006a00',
            shortName: 'AIR available',
            buttonType: 'label-success'
        },
        'Aircraft Inspection in progress': {
            calendarColor: '#004f4f',
            shortName: 'In Progress',
            buttonType: 'label-info'
        },
        'Aircraft Inspection Report in progress': {
            calendarColor: '#006b6b',
            shortName: 'AIR In Progress',
            buttonType: 'label-info'
        },
        'AIR under revision': {
            calendarColor: '#00d700',
            shortName: 'Under Revision',
            buttonType: 'label-info'
        },
        'Requested by Client': {
            calendarColor: '#ff8017',
            shortName: 'Requested',
            buttonType: 'label-default'
        }
    },

    makeList: function (listName, entryToUpdate) {
        entryToUpdate = entryToUpdate || null;
        $.ajax({
            type: 'post',
            url: apiUrl+'list/'+listName+'.php',
            beforeSend: function (xhr) {
                //JWT = decodeURIComponent(window.location.search.substr(1).split("=")[1]);
                xhr.setRequestHeader ("Authorization", "Bearer " + EyesOn.common.JWT);
            },
            success: function (result) {
                var data = JSON.parse(result);

                if (data.hasOwnProperty('error')) {
                    swal("Error", "Server error on "+listName+" list request", "error");
                } else {

                    EyesOn.common.fillList(data, listName, entryToUpdate);
                }
            }
        });
    },

    fillList: function (data, listName, entryToUpdate) {
        function sort(a,b){
            a = a.text.toLowerCase();
            b = b.text.toLowerCase();
            if(a > b) {
                return 1;
            } else if (a < b) {
                return -1;
            }
            return 0;
        }

        var select_list = $('.eo-list-'+listName);
        select_list.empty();

        var dataNormalized = data.map(function (item) {
            return {
                id: item.id,
                name: item.name ||
                      item.aerodrome ||
                      item.title ||
                      item.status
            };
        });

        if (entryToUpdate !== null && entryToUpdate !== undefined) {
            var e;

            if (listName === 'inspection_status') {
                listName = 'status';
            }

            //select_list.append($('<option />'));
            switch (listName){
                case 'scope':
                    e = $.grep(data, function (item) {
                        return entryToUpdate['services'].includes(item.name);
                    });
                    $.each(data, function() {
                        select_list.append($('<option />').val(this.id).text(this.name));
                    });
                    var selectListOption = $('.eo-list-'+listName+' option');
                    selectListOption.sort(sort);
                    select_list.html(selectListOption);
                    select_list.val(e.map(function (item) { return item.id; }));
                    break;
                case 'employee':
                    var empList = entryToUpdate[listName].map(function (item) {return item.name;});
                    e = $.grep(data, function (item) {
                        return empList.includes(item.name);
                    });
                    $.each(data, function() {
                        select_list.append($('<option />').val(this.id).text(this.name));
                    });
                    var selectListOption = $('.eo-list-'+listName+' option');
                    selectListOption.sort(sort);
                    select_list.html(selectListOption);
                    select_list.val(e.map(function (item) { return item.id; }));
                    break;
                case 'status':
                    if (!entryToUpdate.hasOwnProperty('status')){
                        break;
                    }
                default:
                    e = $.grep(dataNormalized, function (item) {
                        return item.name == entryToUpdate[listName];
                    });
                    $.each(dataNormalized, function() {
                        select_list.append($('<option />').val(this.id).text(this.name));
                    });

                    if (listName === 'status') {
                        listName = 'inspection_status';
                    }

                    var selectListOption = $('.eo-list-'+listName+' option');
                    selectListOption.sort(sort);
                    var newSelectListOption = [];

                    newSelectListOption.push(selectListOption[0]);
                    for (var i = 0; i < selectListOption.length - 1; i++) {
                        if (selectListOption[i].innerText != selectListOption[i + 1].innerText) {
                            newSelectListOption.push(selectListOption[i+1]);
                        }
                    };

                    select_list.html(newSelectListOption);
                    select_list.val(e[0].id);

            }
            
        } else {
            $.each(dataNormalized, function() {
                select_list.append($('<option />').val(this.id).text(this.name));
            });

            var selectListOption = $('.eo-list-'+listName+' option');
            selectListOption.sort(sort);
            select_list.html(selectListOption);

            if (listName === 'country'){
                // Select 'Brazil' as country list default
                var e;
                e = $.grep(data, function (item) {
                    return item.name == 'Brazil';
                });
                select_list.val(e[0].id);
            }

        }
        
        select_list.select2({allowClear: true, placeholder: 'Select', width: '100%'});
        if (entryToUpdate === null || entryToUpdate === undefined) {
            select_list.select2("val", "");
        }

    },

    fillAircraftList: function (sourceObj) {
        sourceObj = sourceObj || null;

        function formatRepo (aircraft) {
            if (aircraft.loading) return aircraft.text;

            var markup = "" +
            "<div class='select2-result-aircraft clearfix'>" +
                "<div class='select2-result-aircraft-mark'>" + 
                    "<i class='fa fa-plane'></i> " + aircraft.registry + 
                "</div>" +
                "<div class='select2-result-aircraft-model'>" + 
                    "<b>Model</b>: " + aircraft.modelo +
                "</div>" +
                "<div class='select2-result-aircraft-serial'>" +
                    "<b>S/N</b>: " + aircraft.numero_de_serie +
                "</div>" +
            "</div>";

            return markup;
        }

        function formatRepoSelection (aircraft) {
            return aircraft.registry || aircraft.text;
        }

        var aircraft_list = $('.eo-list-aircraft');

        if (sourceObj != null && sourceObj !== undefined) {
            for (var i = 0; i < sourceObj.aircraft.length; i++) {
                var $option = $('<option />').val(sourceObj.aircraft[i].id);
                $option.text(sourceObj.aircraft[i].registry);
                aircraft_list.append($option);
            };
        };

        aircraft_list.select2({
            ajax: {
                url: apiUrl+"list/aircraft.php",
                type: 'post',
                dataType: 'json',
                delay: 250,
                data: function (params) {
                    return {
                        q: params.term, // search term
                    };
                },
                beforeSend: function (xhr) {
                    //JWT = decodeURIComponent(window.location.search.substr(1).split("=")[1]);
                    xhr.setRequestHeader ("Authorization", "Bearer " + EyesOn.common.JWT);
                },
                processResults: function (data, params) {
                    return {
                        results: data
                    };
                },
                cache: true
            },
            escapeMarkup: function (markup) { return markup; },
            minimumInputLength: 3,
            templateResult: formatRepo,
            templateSelection: formatRepoSelection,
            language: {
                inputTooShort: function() {
                    return 'Please enter the first 3 characters of a registry or serial number without hifen';
                }
            }
        });

        if (sourceObj != null && sourceObj !== undefined) {
            aircraft_list.val(sourceObj.aircraft.map(function (item) {return item.id})).trigger('change');
        };
    },

    loadFormElements: function (sourceObj) {
        sourceObj = sourceObj || null;

        // Populate form lists of available countries, bases and companies
        var commonLists = ['country',
                           'base',
                           'operator',
                           'employee',
                           'company',
                           'commercial_condition',
                           'scope',
                           'inspection_status'];
        for (var i = 0; i < commonLists.length; i++) {
            if($('.eo-list-'+commonLists[i])[0]) {
                EyesOn.common.makeList(commonLists[i], sourceObj);
            }
        };

        if ($('.eo-list-aircraft')[0]){
            EyesOn.common.fillAircraftList(sourceObj);
        }

        // fill inputs if this is an update form
        if (sourceObj != null) {
            for (var property in sourceObj) {
                if (sourceObj.hasOwnProperty(property)) {

                    if (property === 'description' || property === 'other') {
                        $('.eo-text-'+property).html(sourceObj[property])
                    } else {

                        var input_box = $('.eo-input-'+property);
                        input_box.val(sourceObj[property]);
                        if (property === 'tel') {
                            input_box.attr('placeholder', sourceObj[property]);
                        }

                        var static_info = $('.eo-static-'+property);
                        static_info.text(sourceObj[property]);
                    }

                }
            }

        }

        if ($('.i-checks')[0]){
            $('.i-checks').iCheck({
                checkboxClass: 'icheckbox_square-green',
                radioClass: 'iradio_square-green',
            });
        }

        if ($('.input-daterange')) {
            $('#dates .input-daterange').datepicker({});
        }

        function initializeSummernote(placeholder) {
            $('.summernote, .summernote_AIA').summernote({
                placeholder: placeholder,
                toolbar: [
                    ['style', ['bold', 'italic', 'underline', 'clear']],
                    ['font', ['strikethrough', 'superscript', 'subscript']],
                    ['fontsize', ['fontsize']],
                    ['color', ['color']],
                    ['para', ['ul', 'ol', 'paragraph']]
                ],
                cleaner:{
                    notTime: 2400, // Time to display Notifications.
                    action: 'both', // both|button|paste 'button' only cleans via toolbar button, 'paste' only clean when pasting content, both does both options.
                    newline: '<br>', // Summernote's default is to use '<p><br></p>'
                    notStyle: 'position:absolute;top:0;left:0;right:0', // Position of Notification
                    icon: '<i class="note-icon">[Your Button]</i>',
                    keepHtml: false, // Remove all Html formats
                    keepOnlyTags: ['<p>', '<br>', '<ul>', '<li>', '<b>', '<strong>','<i>', '<a>'], // If keepHtml is true, remove all tags except these
                    keepClasses: false, // Remove Classes
                    badTags: ['style', 'script', 'applet', 'embed', 'noframes', 'noscript', 'html'], // Remove full tags with contents
                    badAttributes: ['style', 'start'] // Remove attributes from remaining tags
                }
            });
        }

        if ($('.summernote')[0]){
            initializeSummernote('Insert your description here...');
        }
        
         if ($('.summernote_AIA')[0]){
            initializeSummernote('Please write the message to be sent with the AIA message here...');
        }

        if($('.eo-input-tel')[0]){
            var phoneCodesSrc = "js/plugins/inputmask-multi/data/phone-codes.json";
            var maskList = $.masksSort($.masksLoad(phoneCodesSrc), ['#'], /[0-9]|#/, "mask");
            var maskOpts = {
                inputmask: {
                    definitions: {
                        '#': {
                            validator: "[0-9]",
                            cardinality: 1
                        }
                    },
                    showMaskOnHover: false,
                    autoUnmask: false,
                    clearMaskOnLostFocus: false
                },
                match: /[0-9]/,
                replace: '#',
                list: maskList,
                listKey: "mask",
                onMaskChange: function(maskObj, determined) {
                    if (determined) {
                        var hint = maskObj.name_en;
                        if (maskObj.desc_en && maskObj.desc_en != "") {
                            hint += " (" + maskObj.desc_en + ")";
                        }
                        $("#descr").html(hint);
                    } else {
                        $("#descr").html("Mask of input");
                    }
                }
            };

            $('.eo-input-tel').inputmasks(maskOpts);
        }
    },



    // Paints the status in inspection tables with different colors
    prettifyInspectionsLabels: function () {

        function addColors (index, oldHtml) {
            
            var link = $('<a />').addClass('label change_inspection_button').attr('id', $(this).attr('id'));

            var status = EyesOn.common.inspectionStatus[oldHtml];
            link.text(status.shortName).addClass(status.buttonType);


            if($('#eo-profile').text() === 'Admin' || $('#eo-profile').text() === 'Project Manager' ){
                link.attr('href', "#eo-modal-change-status");
                link.attr('data-toggle', "modal");
            }
            return link;

        }

        $('.inspection-status').html(addColors);
        $('.client_inspection-status').html(addColors);
        $('.insp_inspection-status').html(addColors);
    },
};

EyesOn.events = {
    errorAlert: function (server_response) {
        swal("Error",
             server_response,
             "error");
    },
    successAlert: function (operation, entity_name) {
        swal("Success",
             entity_name+" has been "+operation+"d.",
             "success");
    },
    successPreviewAlert: function (response) {
        swal("Success",
             response,
             "success");
    }
};

/*****************************************************************************/
/*****************************************************************************/
/*****************************************************************************/
var onlyOpenInspections = true;

var Conf = function (name) {
    this.name = name;
};

Conf.prototype.initializeCRUD = function () {
    var self = this;
    $('#eo-page-content').load(
        'view/'+this.name+'.html',
        function () {
            self.initializeTable();
        }
    );
    $('.eo-floating').show();
};

Conf.prototype.initializeTable = function () {
    var self = this;
    // toggle loading page
    $('.ibox-content').toggleClass('sk-loading');

    // Show only open inspections if checkbox is marked
    $('#eo-open-inspections-filter').prop('checked', onlyOpenInspections);
    $('#eo-open-inspections-filter').click(function() {
        if($('#eo-open-inspections-filter').prop('checked')) {
            onlyOpenInspections = true;
        } else {
            onlyOpenInspections = false;
        }
        self.initializeCRUD();
    });

    // make request for a list of existing entries
    $.ajax({
        url: apiUrl+'list/'+self.name+'.php',
        type: 'post',
        beforeSend: function (xhr) {
            xhr.setRequestHeader ("Authorization", "Bearer " + EyesOn.common.JWT);
        },
        data: {only_open: onlyOpenInspections},
        success: function (response) {
            
            var data = JSON.parse(response);
            if (data.hasOwnProperty('error')) {
                // If server returns an error, pop it to the screen
                swal("Error", "Server error on "+self.name+" list request", "error");
            } else {
                // If server returns a operation with success, fill the page table
                var tableContent = $('#eo-table-content');

                for (var i = 0; i < data.length; i++) {
                    var row = self.makeTableRow(data[i]);
                    tableContent.append(row);
                };
            }

            // load Footable plugin
            $('.footable').footable();
            // Paging element of FooTable only works with this workaround
            if ($('.paging > tr > td').css('display') == 'none'){
               $('.paging > tr > td').show();
            }

            EyesOn.common.prettifyInspectionsLabels();

            self.configureTableButtons(data);
            $('.ibox-content').toggleClass('sk-loading');
        },
        error: self.manageResponseOnFailure.bind(self)
    });
};

// Prepares a table row given an JSON entry
Conf.prototype.makeTableRow = function(entry) {
    var $tr = $('<tr />');

    for (var property in entry) {
        if (entry.hasOwnProperty(property) && property !== 'id') {
            $tr.append(this.makeCellWithProperty(property, entry[property], entry.id));
        } else if (property === 'id') {
            $tr.attr('id', entry.id);
        }
    }

    var $td = $('<td />').addClass(this.name+'-action');

    // Inspectors and clients shouldn't be able to update an inspection
    if(this.name !== 'insp_inspection' && this.name !== 'client_inspection'){
        $td.append(this.makeUpdateBtn(entry.id));
    }
    // Inspectors and clients shouldn't be able to update an inspection
    if(this.name === 'client_inspection' && entry.status === 'Requested by Client'){
        $td.append(this.makeUpdateBtn(entry.id));
    }
    // Inspectors and clients shouldn't be able to update an inspection
    if(this.name === 'insp_inspection' && entry.status !== 'Requested by Client'){
        $td.append(this.makeInspectionBtn(entry.id));
    }

    // Inspection table has special buttons
    if (this.name === 'inspection'){
        $td.append($('<span />').text(' '));
        $td.append(this.makeCopyBtn(entry.id));

        $td.append($('<span />').text(' '));
        $td.append(this.makeAiaBtn(entry.id));
    }

    $tr.append($td);
    return $tr;
};

Conf.prototype.makeCellWithProperty = function(propertyName, propertyValue, entryId) {
    var $td = $('<td />');
    $td.addClass(this.name+'-'+propertyName);

    switch (propertyName) {
        case 'valid':
            if(propertyValue) {
                $td.append('<i class="fa fa-check"></i>');
            } else {
                $td.append('<i class="fa fa-times"></i>');
            }
            break;
        case 'aircraft':
            var aircraftList = '';
            for (var j = 0; j < propertyValue.length; j++) {
                aircraftList += ' ' + propertyValue[j].registry + ',';
            };
            aircraftList = aircraftList.replace(/,\s*$/, "");
            $td.text(aircraftList);
            break;
        case 'employee':
            var $ul = $('<ul />');
            for (var j = 0; j < propertyValue.length; j++) {
                $ul.append('<li>'+propertyValue[j].name+'</li>');
            };
            $td.append($ul);
            break;
        case 'status':
            $td.attr('id', 'change_' + entryId);
            $td.text(propertyValue);
            break;
        case 'services':
            var $ul = $('<ul />');
            for (var j = 0; j < propertyValue.length; j++) {
                $ul.append('<li>'+propertyValue[j]+'</li>');
            };
            $td.append($ul);
            break;
        default:
            $td.html(propertyValue);
    }

    return $td;
};

// Adds handlers for all buttons in main view
Conf.prototype.configureTableButtons = function (data) {
    var self = this;
    $('.eo-add-btn').off();
    $('.update_'+self.name+'_button').off();
    $('.copy_'+self.name+'_button').off();
    $('.inspect_button').off();
    $('.aia_button').off();
    $('.change_inspection_button').off();

    // Prepare inclusion button
    $('.eo-add-btn').click(function () {
        $.ajax({
            url: 'add_form/'+self.name+'.html',
            success: function (response) {
                self.initializeInsertionForm(response);
            },
            error: self.manageResponseOnFailure.bind(self)
        });
    });

    // Prepare update buttons
    $('.update_'+self.name+'_button').click(function () {
        
        var updateId = $(this).attr('id').split('_')[1];
        $.ajax({
            type: 'GET',
            url: 'update_form/'+self.name+'.html',
            success: function (response) {
                var entryToUpdate = self.getEntryById(updateId, data);
                self.initializeUpdateForm(response, entryToUpdate);
            },
            error: self.manageResponseOnFailure.bind(self)
        });

    });


    // Prepare copy button
    $('.copy_'+self.name+'_button').click(function () {

        var copyId = $(this).attr('id').split('_')[1];
        $.ajax({
            url: 'add_form/'+self.name+'.html',
            success: function (response) {
                var entryToCopyFrom = self.getEntryById(copyId, data);
                self.initializeInsertionForm(response, entryToCopyFrom);
            },
            error: self.manageResponseOnFailure.bind(self)
        });

    });

    // AIA button
    $('.aia_button').click(function () {
        
        var updateId = $(this).attr('id').split('_')[1];
        $.ajax({
            type: 'GET',
            url: 'AIA/AIA.html',
            success: function (response) {
                var entryToUpdate = self.getEntryById(updateId, data);
                self.initializeUpdateForm(response, entryToUpdate);
            },
            error: self.manageResponseOnFailure.bind(self)
        }).done(function (response) {
            var entryToUpdate = self.getEntryById(updateId, data);
            self.initializeAIAForm(response, entryToUpdate);
        });

    });

    // Inspection button
    $('.inspect_button').click(function () {
        
        var updateId = $(this).attr('id').split('_')[1];
        $.ajax({
            type: 'GET',
            url: 'inspect/inspect.html',
            success: function (response) {
                var entryToUpdate = self.getEntryById(updateId, data);
                self.initializeUpdateForm(response, entryToUpdate);
            },
            error: self.manageResponseOnFailure.bind(self)
        }).done(function (response) {
            var entryToUpdate = self.getEntryById(updateId, data);
            self.initializeInspectionForm(response, entryToUpdate);
        });

    });

    // Prepare 'Change Status Button'
    if($('#eo-profile').text() === 'Admin' || $('#eo-profile').text() === 'Project Manager' ){
        $('.change_inspection_button').click(function () {
            
            var updateId = $(this).attr('id').split('_')[1];
            var entryToUpdate = self.getEntryById(updateId, data);
            $('.eo-confirm-btn').off();

            $('#eo-air-num-wrapper').hide();
            $('.eo-list-inspection_status').change(function(){
                if ($(this).find(':selected').val() == 6) {
                    $('#eo-air-num-wrapper').slideDown();
                } else {
                    $('#eo-air-num-wrapper').slideUp();
                }
            });

            // Load plugins and auto-populated elements
            EyesOn.common.loadFormElements(entryToUpdate);
            $('.eo-confirm-btn').click(function () {
                if($('.eo-list-inspection_status').val()){
                    $('.modal-body').toggleClass('sk-loading');

                    var sendData = 'id=' + updateId + '&' + $('#eo-inspection-status-modal-form').serialize();
                    $('.eo-list-inspection_status').prop('disabled', 'disabled');
                    $.ajax({
                        url: apiUrl+'update/'+self.name+'.php',
                        method: 'POST',
                        beforeSend: function (xhr) {
                            //JWT = decodeURIComponent(window.location.search.substr(1).split("=")[1]);
                            xhr.setRequestHeader ("Authorization", "Bearer " + EyesOn.common.JWT);
                        },
                        data: sendData,
                        success : function (response) {
                            self.manageConfirmResponseOnUpdateSuccess(response);
                            $('.modal-body').toggleClass('sk-loading');
                            $('.eo-list-inspection_status').prop('disabled', false)
                            $('#eo-modal-change-status').modal('toggle');
                        },
                        error: function (response) {
                            self.manageResponseOnFailure(response);
                            $('.modal-body').toggleClass('sk-loading');
                            $('.eo-list-inspection_status').prop('disabled', false)
                            $('#eo-modal-change-status').modal('toggle');
                        }
                    });
                }
            });
            
            // On Cancel Button click, return to main table
            $('.eo-cancel-btn').attr('href', "#eo-modal-change-status");
            $('.eo-cancel-btn').attr('data-toggle', "modal");

        });
    }
};

Conf.prototype.makeUpdateBtn = function(entryId) {
    var $updateBtn = $('<button />').attr('title', 'Update');
    $updateBtn.attr('id', 'update_' + entryId);
    $updateBtn.addClass('ladda-button btn btn-circle btn-info');
    $updateBtn.addClass('update_'+this.name+'_button');
    $updateBtn.append('<i class="fa fa-pencil"></i>');
    return $updateBtn;
};

Conf.prototype.makeCopyBtn = function(entryId) {
    var $copyBtn = $('<button />').attr('title', 'Reschedule');
    $copyBtn.attr('id', 'copy_' + entryId);
    $copyBtn.addClass('ladda-button btn btn-circle btn-warning');
    $copyBtn.addClass('copy_'+this.name+'_button');
    $copyBtn.append('<i class="fa fa-repeat"></i>');
    return $copyBtn;
};

Conf.prototype.makeAiaBtn = function(entryId) {
    var $aiaBtn = $('<button />').attr('title', 'Send AIA');
    $aiaBtn.attr('id', 'aia_' + entryId);
    $aiaBtn.addClass('ladda-button btn btn-danger btn-sm');
    $aiaBtn.addClass('aia_button');
    $aiaBtn.text('Send AIA');
    return $aiaBtn;
};

Conf.prototype.makeInspectionBtn = function(entryId) {
    var $inspectionBtn = $('<button />').attr('title', 'Send AIA');
    $inspectionBtn.attr('id', 'inspect_' + entryId);
    $inspectionBtn.addClass('ladda-button btn btn-circle btn-danger btn-sm');
    $inspectionBtn.addClass('inspect_button');
    $inspectionBtn.append('<i class="fa fa-search"></i>');
    return $inspectionBtn;
};

Conf.prototype.initializeAIAForm = function (form, entryToUpdate) {
    // Save reference to this CRUD instance
    var self = this;

    //Hide floating add button
    $('.eo-floating').hide();

    // Load form
    $('#eo-page-content').html(form);

    // Load plugins and auto-populated elements
    EyesOn.common.loadFormElements(entryToUpdate);
    
    // On Confirm Button click, send form info to update.php
    $('.eo-confirm-btn').click(function () {
        // toggle loading page
        $('.ibox-content').toggleClass('sk-loading');

        $.ajax({
            url: apiUrl+'AIA/AIA.php',
            method: 'POST',
            beforeSend: function (xhr) {
                //JWT = decodeURIComponent(window.location.search.substr(1).split("=")[1]);
                xhr.setRequestHeader ("Authorization", "Bearer " + EyesOn.common.JWT);
            },
            data: self.serializeUpdateForm(entryToUpdate.id),
            success : function (response) {
                var data = JSON.parse(response);

                if (data.hasOwnProperty('error')) {
                    EyesOn.events.errorAlert(data.error);
                } else {
                    $('.ibox-content').toggleClass('sk-loading');
                    EyesOn.events.successAlert('update', self.name);
                    self.initializeCRUD();
                }
            },
            error: function (response) {
                $('.ibox-content').toggleClass('sk-loading');
                EyesOn.events.errorAlert(response);
            }
        });
        
    });

    $('.eo-preview-btn').click(function () {
        $('.ibox-content').toggleClass('sk-loading');

        $.ajax({
            url: apiUrl+'AIA/AIA_preview.php',
            method: 'POST',
            beforeSend: function (xhr) {
                //JWT = decodeURIComponent(window.location.search.substr(1).split("=")[1]);
                xhr.setRequestHeader ("Authorization", "Bearer " + EyesOn.common.JWT);
            },
            data: self.serializeUpdateForm(entryToUpdate.id),
            success : function (response) {
                var data = JSON.parse(response);

                if (data.hasOwnProperty('error')) {
                    EyesOn.events.errorAlert(data.error);
                } else {
                    $('.ibox-content').toggleClass('sk-loading');
                    EyesOn.events.successPreviewAlert(data.success);
                    self.initializeCRUD();
                }
            },
            error: function (response) {
                $('.ibox-content').toggleClass('sk-loading');
                EyesOn.events.errorAlert(response);
            }
        });
    });
    
    // On Cancel Button click, return to main table
    $('.eo-cancel-btn').click(function () {
        self.initializeCRUD();
    });

};

Conf.prototype.initializeInspectionForm = function (form, entryToUpdate) {
    // Save reference to this CRUD instance
    var self = this;

    //Hide floating add button
    $('.eo-floating').hide();

    $('.eo-photo-physical-btn').click(function () {
        navigator.camera.getPicture(self.photoSuccessPhysical.bind(self), self.photoError, {destinationType: Camera.DestinationType.DATA_URL});
    });

    $('.eo-gallery-physical-btn').click(function () {
        navigator.camera.getPicture(self.photoSuccessPhysical.bind(self), self.photoError, {sourceType: Camera.PictureSourceType.PHOTOLIBRARY, destinationType: Camera.DestinationType.DATA_URL});
    });

    $('.eo-photo-document-btn').click(function () {
        navigator.camera.getPicture(self.photoSuccessDocument.bind(self), self.photoError, {destinationType: Camera.DestinationType.DATA_URL});
    });
    
    $('.eo-gallery-document-btn').click(function () {
        navigator.camera.getPicture(self.photoSuccessDocument.bind(self), self.photoError, {sourceType: Camera.PictureSourceType.PHOTOLIBRARY, destinationType: Camera.DestinationType.DATA_URL});
    });

    $('.eo-confirm-btn').off().click(function (){
        self.sendPhotos(entryToUpdate.id);
    });
    // On Cancel Button click, return to main table
    $('.eo-cancel-btn').click(function () {
        self.initializeCRUD();
    });

};

Conf.prototype.photoSuccessPhysical = function(image) {
    if (window.mobilecheck()){
        this.photoMobileSuccess(image, 'physical');
    } else {
        this.photoDesktopSuccess(image, 'physical');
    }
};

Conf.prototype.photoSuccessDocument = function(image) {
    if (window.mobilecheck()) {
        this.photoMobileSuccess(image, 'document');
    } else {
        this.photoDesktopSuccess(image, 'document');
    }
};
/*
Conf.prototype.photoMobileSuccess = function(imageURI, type) {
    $('#eo-gallery-' + type).append(this.makePhotoItem(imageURI, type));
};
*/
Conf.prototype.photoMobileSuccess = function(base64image, type) {
    $('#eo-gallery-' + type).append(this.makePhotoItem('data:image/jpeg;base64,'+base64image, type));
};

Conf.prototype.photoDesktopSuccess = function(base64image, type) {
    $('#eo-gallery-' + type).append(this.makePhotoItem('data:image/jpeg;base64,'+base64image, type));
};

Conf.prototype.makePhotoItem = function(image, type) {
    var $divContainer = $('<div />').addClass('col-xs-6 col-sm-6 col-md-4 col-lg-3 m-t-sm eo-photo-div-container');
    $divContainer.css('padding', '5px');

    var $imgContainer = $('<div />').addClass('ibox-content text-center');
    $imgContainer.css('border-style', 'solid solid');
    $imgContainer.css('border-width', '1px 1px');
    $imgContainer.css('border-radius', '10px');
    $imgContainer.css('padding', '7px');

    var $img = $('<img />').attr('src', image).addClass('eo-img-'+type).css('width', '100%');

    var $btn = $('<button />').addClass('btn btn-danger btn-outline btn-circle');
    $btn.append($('<i />').addClass('fa fa-times'));
    $btn.click(function () {
        $(this).closest('.eo-photo-div-container').remove();
    });

    //var $upIcon = $('<i />').addClass('fa fa-check').addClass('m-l-sm');

    var $btnContainer = $('<div />').addClass('m-t-xs');
    $btnContainer.append($btn);//.append($upIcon);

    $divContainer.append($imgContainer.append($img).append($('<div />').addClass('m-t-sm').append($btn)));

    return $divContainer;
};

Conf.prototype.photoError = function(errorMessage) {
    EyesOn.events.errorAlert(errorMessage);
};

Conf.prototype.sendPhotos = function(inspectionId) {
    var self = this;

    var physicalPhotos = $('.eo-img-physical').map(function(){return $(this).attr('src');}).get();
    var documentPhotos = $('.eo-img-document').map(function(){return $(this).attr('src');}).get();

    physicalPhotos = $('.eo-img-physical').map(function(){return $(this).attr('src');}).get();
    documentPhotos = $('.eo-img-document').map(function(){return $(this).attr('src');}).get();
    
    console.log(physicalPhotos);
    console.log(documentPhotos);
    
    $.ajax({
        url: apiUrl+'inspect/photos.php',
        type: 'post',
        data: {
            inspId: inspectionId,
            physicalPhotos: physicalPhotos,
            documentPhotos: documentPhotos
        },
        beforeSend: function (xhr) {
            xhr.setRequestHeader("Authorization", "Bearer " + EyesOn.common.JWT);
        },
        success: function (response) {
            swal('Success', response.message, 'success');
        },
        error: function (jqXHR) {
            swal('Error', jqXHR.responseJSON.error, 'error');
        }
    });
};

Conf.prototype.loadImage = function (src){
    var reader = new FileReader();
    reader.readAsDataURL(src);

    return reader.result;
}

Conf.prototype.initializeUpdateForm = function (form, entryToUpdate) {
    // Save reference to this CRUD instance
    var self = this;

    //Hide floating add button
    $('.eo-floating').hide();

    // Load form to page
    $('#eo-page-content').html(form);

    // Load plugins and auto-populated elements
    EyesOn.common.loadFormElements(entryToUpdate);

    $('.eo-list-inspection_status').change(function(){
        if ($(this).find(':selected').val() == 6) {
            $('#eo-air-num-wrapper').slideDown();
        } else {
            $('#eo-air-num-wrapper').slideUp();
        }
    });
    
    // On Confirm Button click, send form info to update.php
    $('.eo-confirm-btn').click(function () {
        $('.ibox-content').toggleClass('sk-loading');
        $.ajax({
            url: apiUrl+'update/'+(self.name == 'client_inspection' ? 'inspection' : self.name) +'.php',
            method: 'POST',
            beforeSend: function (xhr) {
                //JWT = decodeURIComponent(window.location.search.substr(1).split("=")[1]);
                xhr.setRequestHeader ("Authorization", "Bearer " + EyesOn.common.JWT);
            },
            data: self.serializeUpdateForm(entryToUpdate.id),
            success: self.manageConfirmResponseOnUpdateSuccess.bind(self),
            error: self.manageResponseOnFailure.bind(self),
            complete: function () {
                $('.ibox-content').toggleClass('sk-loading');
            }
        });
    });

    // On Delete Button click, send form info to delete.php
    $('.eo-remove-btn').click(function (e) {
        self.alertRemovalConfirmDialog(entryToUpdate.id);
    });
    
    // On Cancel Button click, return to main table
    $('.eo-cancel-btn').click(function () {
        self.initializeCRUD();
    });
};


// Loads an EyesOn CRUD insertion form
Conf.prototype.initializeInsertionForm = function (form, entryToCopyFrom) {
    entryToCopyFrom = entryToCopyFrom || null;
    // Save reference to this CRUD instance
    var self = this;

    //Hide floating add button
    $('.eo-floating').hide();

    // Load form to page
    $('#eo-page-content').html(form);

    if (self.name === 'inspection'|| self.name === 'client_inspection') {
        self.loadStaticHistory();
    }

    // Load plugins and auto-populated elements
    EyesOn.common.loadFormElements(entryToCopyFrom);

    // On Confirm Button click, send form info to insert/entity.php
    $('.eo-confirm-btn').off().click(function () {
        $('.ibox-content').toggleClass('sk-loading');
        $.ajax({
            url: apiUrl+'insert/'+self.name+'.php',
            method: 'post',
            beforeSend: function (xhr) {
                //JWT = decodeURIComponent(window.location.search.substr(1).split("=")[1]);
                xhr.setRequestHeader ("Authorization", "Bearer " + EyesOn.common.JWT);
            },
            data: self.serializeInsertionForm(),
            success : function (response) {
                $('.ibox-content').toggleClass('sk-loading');
                self.manageConfirmResponseOnInsertionSuccess(response);
            },
            error: self.manageResponseOnFailure.bind(self)
        });
    });

    // On Cancel Button click, return to main view
    $('.eo-cancel-btn').click(function () {
        self.initializeCRUD();
    });
};

// Recovers a element of a list that has a given id
Conf.prototype.getEntryById = function(entryId, data) {
    for (var i = 0; i < data.length; i++) {
        if (data[i].id == entryId) {
            return data[i];
        };
    };

    return null;
};

// Pops up a SweetAlert removal confirm dialog
Conf.prototype.alertRemovalConfirmDialog = function (entryId) {
    var self = this;
    swal(
        {
            title: "Are you sure?",
            text: "This "+self.name+" will be removed permanentely from the database",
            type: "warning",
            showCancelButton: true,
            confirmButtonColor: "#DD6B55",
            confirmButtonText: "Remove",
            cancelButtonText: "Cancel",
            closeOnConfirm: false,
            closeOnCancel: true
        },
        function (isConfirm) {
            if (isConfirm) {
                $.ajax({
                    type : 'POST',
                    beforeSend: function (xhr) {
                        //JWT = decodeURIComponent(window.location.search.substr(1).split("=")[1]);
                        xhr.setRequestHeader ("Authorization", "Bearer " + EyesOn.common.JWT);
                    },
                    url : apiUrl+'remove/'+self.name+'.php',
                    data : {id: entryId},
                    success : self.manageConfirmResponseOnRemoveSuccess.bind(self),
                    error: self.manageResponseOnFailure.bind(self)
                });
            }
        }
    );
};

// Handles server 200 status. Requires '.bind(this)' when used as callback
Conf.prototype.manageConfirmResponseOnInsertionSuccess = function (serverResponse) {
    this.manageConfirmResponseOnSuccess(serverResponse, 'create');
};

// Handles server 200 status. Requires '.bind(this)' when used as callback
Conf.prototype.manageConfirmResponseOnUpdateSuccess = function (serverResponse) {
    this.manageConfirmResponseOnSuccess(serverResponse, 'update');
};

// Handles server 200 status. Requires '.bind(this)' when used as callback
Conf.prototype.manageConfirmResponseOnRemoveSuccess = function (serverResponse) {
    this.manageConfirmResponseOnSuccess(serverResponse, 'remove');
};

Conf.prototype.manageConfirmResponseOnSuccess = function (serverResponse, type) {
    var dataReceived = JSON.parse(serverResponse);

    if (dataReceived.hasOwnProperty('error')) {
        EyesOn.events.errorAlert(dataReceived.error);
    } else {
        EyesOn.events.successAlert(type, this.name);
        this.initializeCRUD();
    };
}

// Handles server ~200 status. Requires '.bind(this)' when used as callback
Conf.prototype.manageResponseOnFailure = function (serverResponseObject) {
    EyesOn.events.errorAlert(serverResponseObject.responseJSON.error);
};

// Serializes all elements in an insertion form
Conf.prototype.serializeInsertionForm = function () {
    var dataToSend = $('#add_'+this.name+'_form').serialize() || $('#update_'+this.name+'_form').serialize();

    if ($('.summernote')[0]) {
        dataToSend += '&description='+encodeURIComponent($('.summernote').summernote('code'));
        console.log(dataToSend);
    };
    
    if ($('.summernote_AIA')[0]) {
        dataToSend += 'description='+encodeURIComponent($('.summernote_AIA').summernote('code'));
    };

    return dataToSend;
};

// Serializes all elements in an update form
Conf.prototype.serializeUpdateForm = function (entryId) {
    return 'id='+entryId+'&' + this.serializeInsertionForm();
};

Conf.prototype.loadStaticHistory = function() {
    var self = this;
    $.ajax({
        url: apiUrl+'list/inspection.php',
        method: 'post',
        beforeSend: function (xhr) {
            //JWT = decodeURIComponent(window.location.search.substr(1).split("=")[1]);
            xhr.setRequestHeader ("Authorization", "Bearer " + EyesOn.common.JWT);
        },
        success : function (response) {
            var data = JSON.parse(response);

            if (data.hasOwnProperty('error')) {
                swal("Error", "Server error on "+self.name+" list request", "error");
            } else {
                var tableContent = $('.eo-last-inspections');

                for (var i = 0; i < data.length; i++) {
                    tableContent.append(self.makeStaticHistoryRow(data[i]));
                };
            };
        },
        error: function (response) {
            self.events.errorAlert(response);
        }
    });
};

Conf.prototype.makeStaticHistoryRow = function (entry) {
    var $tr = $('<tr />');

    for (var property in entry) {

        if (property === 'aircraft' || property === 'assignment_date') {
            $tr.append(this.makeStaticHistoryCell(property, entry[property]));
        } else if (property === 'id') {
            $tr.attr('id', entry.id);
        }
    }
    return $tr;
}

Conf.prototype.makeStaticHistoryCell = function(propertyName, propertyValue) {
    var $td = $('<td />');
    $td.addClass(this.name+'-'+propertyName);

    switch (propertyName) {                                    
        case 'aircraft':
            var $ul = $('<ul />');
            for (var j = 0; j < propertyValue.length; j++) {
                $ul.append('<li>'+propertyValue[j].registry+'</li>');
            };
            $td.append($ul);
            break;                              
        case 'assignment_date':
            $td.text(propertyValue);
    }

    return $td;
};

/*****************************************************************************/
/*****************************************************************************/
/*****************************************************************************/
// Conf child class for user handling
var UserConf = function (name) {
    Conf.call(this, name);
};

// Extend from parent class prototype
UserConf.prototype = Object.create(Conf.prototype);
UserConf.prototype.constructor = UserConf;


UserConf.prototype.initializeTable = function() {
    var self = this;
    
    $.ajax({
        type: 'post',
        url: apiUrl+'list/user.php',
        beforeSend: function (xhr) {
            //JWT = decodeURIComponent(window.location.search.substr(1).split("=")[1]);
            xhr.setRequestHeader ("Authorization", "Bearer " + EyesOn.common.JWT);
        },
        success: function (result) {

            var data = JSON.parse(result);

            if (data.users.hasOwnProperty('error')) {
                swal("Error", "Server error on users list request", "error");
            } else if (data.pending_users.hasOwnProperty('error')) {
                swal("Error", "Server error on pending users list request", "error");
            } else {

                var tableContent = $('#eo-table-content');
                for (var i = 0; i < data.users.length; i++) {
                    var row = self.makeTableRow(data.users[i]);
                    tableContent.append(row);
                };

                tableContent = $('#eo-table-content-pending');
                for (var i = 0; i < data.pending_users.length; i++) {
                    var row = self.makeTableRow(data.pending_users[i]);
                    tableContent.append(row);
                };
            }

            $('.footable').footable();

            // Paging element of FooTable only works with this workaround
            if ($('.paging > tr > td').css('display') == 'none'){
               $('.paging > tr > td').show();
            }

            self.configureTableButtons(data.users);
        }
    });
};

/*****************************************************************************/
/*****************************************************************************/
/*****************************************************************************/
// Conf child class for calendar handling
var CalendarConf = function (name) {
    Conf.call(this, name);
};

// Extend from parent class prototype
CalendarConf.prototype = Object.create(Conf.prototype);
CalendarConf.prototype.constructor = CalendarConf;

CalendarConf.prototype.initializeCRUD = function () {
    var self = this;
    $('#eo-page-content').load(
        'view/calendar.html',
        function () {
            self.initializeTable();
        }
    );

};

CalendarConf.prototype.initializeTable = function() {
    var self = this;

    if (self.name === 'client_inspection') $('#eo-calendar-filter').hide();

    $.ajax({
        url: apiUrl+'list/inspection.php',
        beforeSend: function (xhr) {
            //JWT = decodeURIComponent(window.location.search.substr(1).split("=")[1]);
            xhr.setRequestHeader ("Authorization", "Bearer " + EyesOn.common.JWT);
        }
    }).done(function (response) {
        
        var inspections = JSON.parse(response);
        var inspCalendarList = [];

        for (var i = inspections.length - 1; i >= 0; i--) {

            var status = EyesOn.common.inspectionStatus[inspections[i].status];

            inspCalendarList.push({
                id: inspections[i].id,
                title: (self.name === 'client_inspection' ? '' : (inspections[i].company + ', ')) +
                       inspections[i].aircraft.map(function (item) {return item.registry;}) +  ', ' +
                       inspections[i].base,
                allDay: true,
                start: new Date(inspections[i].start_date),
                end: new Date(inspections[i].end_date),
                color: status.calendarColor
            });
        };

        self.loadCalendar(inspCalendarList, inspections);
    });
};

CalendarConf.prototype.loadCalendar = function(inspectionsList, data) {
    var self = this;
    
    $('#calendar').fullCalendar({
        header: {
            left: 'prev,next today',
            center: 'title',
            right: 'month,agendaWeek,agendaDay'
        },
        editable: false,
        droppable: false, // this allows things to be dropped onto the calendar
        events: inspectionsList,
        aspectRatio: 3.4,
        eventClick: function(calEvent, jsEvent, view) {
            // Inspectors and clients shouldn't be able to update an inspection
            var updateId = calEvent.id;
            var entryToUpdate = null;

            for (var i = 0; i < data.length; i++) {
                if (data[i].id == updateId) {
                    entryToUpdate = data[i];
                    break;
                };
            };

            if(self.name === 'insp_inspection' ||
               (self.name === 'client_inspection' && entryToUpdate.status !== 'Requested by Client')){
                return
            }

            $.ajax({
                type: 'POST',
                beforeSend: function (xhr) {
                    //JWT = decodeURIComponent(window.location.search.substr(1).split("=")[1]);
                    xhr.setRequestHeader ("Authorization", "Bearer " + EyesOn.common.JWT);
                },
                url: 'update_form/'+self.name+'.html',
                data: {'id': updateId}
            }).done(function (response) {
                self.initializeUpdateForm(response, entryToUpdate);
            }); 
        },
        eventRender: function (event, element, view) {
            var companyName = $('#eo-calendar-filter').val().toLowerCase();

            return event.title.toLowerCase().includes(companyName);
        },
        dayClick: function(date, jsEvent, view) {
          if(view.name == 'month' || view.name == 'basicWeek') {
            $('#calendar').fullCalendar('changeView', 'basicDay');
            $('#calendar').fullCalendar('gotoDate', date);      
          }
        }
    });

    var filterText = "";
    $('#eo-calendar-filter').bind("propertychange change click keyup input paste", function(event){
        // If value has changed
        if (filterText != $(this).val()){
            filterText = $(this).val();
            $('#calendar').fullCalendar('rerenderEvents');
        }
    });
};

/*****************************************************************************/
/*****************************************************************************/
/*****************************************************************************/
// Conf child class for user handling
var AircraftConf = function (name) {
    Conf.call(this, name);
};

// Extend from parent class prototype
AircraftConf.prototype = Object.create(Conf.prototype);
AircraftConf.prototype.constructor = AircraftConf;


AircraftConf.prototype.initializeTable = function(page, searchTerm) {
    var self = this;
    var last_page = 0;

    searchTerm = searchTerm || "";

    $('#eo-table-content').empty();

    if (page === undefined || page === null) {
        page = 0;
    };

    $.ajax({
        type: 'post',
        beforeSend: function (xhr) {
            //JWT = decodeURIComponent(window.location.search.substr(1).split("=")[1]);
            xhr.setRequestHeader ("Authorization", "Bearer " + EyesOn.common.JWT);
        },
        url: apiUrl+'list/aircraft_all.php',
        data: {page: page, query: searchTerm},
        success: function (result) {

            var data = JSON.parse(result);

            if (data.hasOwnProperty('error')) {
                swal("Error", "Server error on list request", "error");
            } else {

                var tableContent = $('#eo-table-content');
                for (var i = 0; i < data.data.length; i++) {
                    var row = self.makeTableRow(data.data[i]);
                    tableContent.append(row);
                };
            }

            $('.footable').footable();

            self.configureTableButtons(data.data);
            
            last_page = data.n_pages;
            $('#eo-table-page').text(page + " of " + data.n_pages);
        }
    });

    $('#eo-aircraft-filter').off().bind("propertychange change click keyup input paste", function(event){
        // If value has changed
        if (searchTerm != $(this).val()) {
            self.initializeTable(0, $(this).val());
        }
    });

    $('.eo-page-start').off().click(function () {
        self.initializeTable(0, searchTerm);
    });

    $('.eo-page-prev').off().click(function () {
        if (page > 0){
            self.initializeTable(page-1, searchTerm);
        }
    });

    $('.eo-page-next').off().click(function () {
        self.initializeTable(page+1, searchTerm);
    });

    $('.eo-page-end').off().click(function () {
        self.initializeTable(last_page, searchTerm);
    });
};
