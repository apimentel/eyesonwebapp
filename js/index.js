/*
 * Licensed to the Apache Software Foundation (ASF) under one
 * or more contributor license agreements.  See the NOTICE file
 * distributed with this work for additional information
 * regarding copyright ownership.  The ASF licenses this file
 * to you under the Apache License, Version 2.0 (the
 * "License"); you may not use this file except in compliance
 * with the License.  You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing,
 * software distributed under the License is distributed on an
 * "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
 * KIND, either express or implied.  See the License for the
 * specific language governing permissions and limitations
 * under the License.
 */
var app = {
    // Application Constructor
    initialize: function() {
        document.addEventListener('deviceready', this.onDeviceReady.bind(this), false);
    },

    // deviceready Event Handler
    //
    // Bind any cordova events here. Common events are:
    // 'pause', 'resume', etc.
    onDeviceReady: function() {
        eyesonEnableLogin();
    },
};

app.initialize();

function eyesonEnableLogin () {
    // Enagle toggling of activities in sidebar items
    $('#eo-login-btn').click(function () {
        $.ajax({
            type: 'post',
            url: apiUrl+'login_verification.php',
            data: $('#eo-login-form').serialize(),
            success: function (response) {
                console.log(response);
                EyesOn.common.JWT = response.token;
                var base64Url = EyesOn.common.JWT.split('.')[1];
                var base64 = base64Url.replace('-', '+').replace('_', '/');
                var payload = JSON.parse(window.atob(base64));
                $('#eo-body').load('home_'+
                    (payload.profile === 'project manager' ? 'manager' : payload.profile)+
                    '.html',
                    null,
                    function () {
                        console.log(payload.profile);
                        $('body').toggleClass('gray-bg');
                        $('body').toggleClass('no-skin-config');
                    }
                );
            },
            error: function (jqXHR) {
                console.log(jqXHR);
                $('#eo-login-error').text(jqXHR.responseJSON.error);
            }
        });
    });
}
