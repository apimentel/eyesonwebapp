$(document).ready(function () {
	$('#eo-logout-btn').click (function () {
        $('#eo-body').load(
            'login.html',
            null,
            function () {
            	EyesOn.common.JWT = '';
                $('body').toggleClass('gray-bg');
                $('body').toggleClass('no-skin-config');
                eyesonEnableLogin();
            }
        );
    });
});
