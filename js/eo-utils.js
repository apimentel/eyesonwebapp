function prepareUsersSidebarItem () {
    prepareSidebarItem('user', new UserConf('user'));
}

function prepareAircraftSidebarItem () {
    prepareSidebarItem('aircraft', new AircraftConf('aircraft'));
}

function prepareCalendarSidebarItem (screenName) {
    screenName = screenName || 'inspection';
    prepareSidebarItem('calendar', new CalendarConf('inspection'));
}

function prepareSidebarItem (name, eoScreen) {
    $('#eo-' + name + '-li').click(function () {
        $('.eo-add-btn').off();
        eoScreen.initializeCRUD();
        return false;
    });   
}

function prepareOrdinarySidebarItems (ordinarySidebarItemNames) {
    for (var i = 0; i < ordinarySidebarItemNames.length; i++) {
        prepareSidebarItem(
            ordinarySidebarItemNames[i],
            new Conf(ordinarySidebarItemNames[i])
        );
    };
}

function authenticateAjaxRequest (xhr) {
    xhr.setRequestHeader ("Authorization", "Bearer " + EyesOn.common.JWT);
}

function requestUserName () {
    $.ajax({
        type: 'get',
        url: apiUrl+'get/this_user.php',
        beforeSend: authenticateAjaxRequest,
        success: function (response) {
            $('#eo-id').text(response.name);
        }
    });
}

function loadMainPageContent (mainScreenName) {
    mainScreenName = mainScreenName || 'inspection';
    var eoScreen = new Conf(mainScreenName);
    eoScreen.initializeCRUD();
}

function prepareBugReportButton () {
    $('#eo-report-bug').click(function () {
        $('.eo-confirm-btn').off().click(function () {
            $('.modal-body').toggleClass('sk-loading');

            var sendDescription = $('#eo-input-description').val();

            var file = document.getElementById('#eo-input-bug_image').files[0]; //Files[0] = 1st file

            var reader = new FileReader();
            reader.readAsDataURL(file);
            reader.onload = shipOffBugReport;
            
            function shipOffBugReport(event) {
                var result = event.target.result;
                var fileName = document.getElementById('#eo-input-bug_image').files[0].name; //Should be 'picture.jpg'

                var dataToSend = {
                    description: sendDescription,
                    file: result,
                    name: fileName
                };

                $.ajax({
                    url: apiUrl+'bug_report.php',
                    method: 'POST',
                    data: dataToSend,
                    beforeSend: authenticateAjaxRequest,
                    success : function (response) {
                        var dataReceived = JSON.parse(response);

                        if (dataReceived.hasOwnProperty('error')) {
                            swal("Error", dataReceived.error, "error");
                        } else {
                            swal("Success", dataReceived.success, "success");
                        };

                        $('.modal-body').toggleClass('sk-loading');
                        $('#eo-modal-report-bug').modal('toggle');
                    },
                    error: function (response) {
                        EyesOn.events.errorAlert(response);
                        $('.modal-body').toggleClass('sk-loading');
                        $('#eo-modal-report-bug').modal('toggle');
                    }
                });
            }
        });

        // On Cancel Button click, return to main table
        $('.eo-cancel-btn').attr('href', "#eo-modal-report-bug");
        $('.eo-cancel-btn').attr('data-toggle', "modal");


    });
}

function prepareSidebarHighlight () {
    // Enable highlight toggling in sidebar items
    $('.eo-sidebar').click(function (event) {
        $('.eo-add-btn').off();
        $('.eo-sidebar').removeClass('active');
        $(this).addClass('active');
    });
}

function prepareUserProfileSidebarItem () {
    $('#eo-profile-li').click(function () {
        $('.eo-add-btn').off();
        call_user_profile();
        return false;
    });
}

function prepareReportSidebarItem () {
    $('#eo-report-li').click(function () {
        $('#eo-page-content').html('<div class="middle-box text-center"><img alt="image" class="m-b-md" src="img/Black.svg" style="width:240px;display:block;margin-right:auto;margin-left:auto"/><h3 class="font-bold">Work in Progress - Coming Soon</h3></div>');
        return false;
    });
}

function prepareMyCompanySidebarItem () {
    $('#eo-my_company-li').click(function () {
        call_user_company();
    });
}
