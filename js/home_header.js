document.write('\
    <meta charset="utf-8">\
    <meta content="width=device-width, initial-scale=1.0" name="viewport">\
    <title>Eyes On</title>\
    <link href="css/bootstrap.min.css" rel="stylesheet">\
    <link href="font-awesome/css/font-awesome.css" rel="stylesheet">\
    <link href="css/plugins/toastr/toastr.min.css" rel="stylesheet">\
    <link href="css/plugins/footable/footable.core.css" rel="stylesheet">\
    <link href="css/plugins/sweetalert/sweetalert.css" rel="stylesheet">\
    <link href="css/plugins/fullcalendar/fullcalendar.css" rel="stylesheet">\
    <link href="css/plugins/fullcalendar/fullcalendar.print.css" rel="stylesheet" media="print">\
    <link href="css/plugins/iCheck/custom.css" rel="stylesheet">\
    <link href="css/plugins/select2/select2.min.css" rel="stylesheet">\
    <link href="css/plugins/datapicker/datepicker3.css" rel="stylesheet">\
    <link href="css/animate.css" rel="stylesheet">\
    <link href="css/plugins/summernote/summernote.css" rel="stylesheet">\
    <link href="css/plugins/summernote/summernote-bs3.css" rel="stylesheet">\
    <link href="css/plugins/jasny/jasny-bootstrap.min.css" rel="stylesheet">\
    <link href="css/style.css" rel="stylesheet">\
    <link href="css/plugins/awesome-bootstrap-checkbox/awesome-bootstrap-checkbox.css" rel="stylesheet">\
    <link href="css/custom.css" rel="stylesheet">\
\
');
