$(document).ready( function(){
    requestUserName();
    loadMainPageContent('insp_inspection');
    prepareBugReportButton();
    prepareSidebarHighlight();
    
    prepareUserProfileSidebarItem();
    
    prepareCalendarSidebarItem('insp_inspection');
    prepareOrdinarySidebarItems(['insp_inspection']);
    prepareReportSidebarItem();
});
