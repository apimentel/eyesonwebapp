$(document).ready(function(){
    requestUserName();

    loadMainPageContent('client_inspection');
    prepareBugReportButton();
    prepareSidebarHighlight();
    
    prepareOrdinarySidebarItems(['client_inspection']);

    prepareCalendarSidebarItem('client_inspection');
    prepareReportSidebarItem();
    prepareUserProfileSidebarItem();
    prepareAircraftSidebarItem();
    prepareMyCompanySidebarItem();
    
});
